var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');
var request = require('request');

var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
  beforeEach(function(done) {
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'conection error;'));
    db.once('open', function() {
      console.log("We are connected to test database");
      done();
    });
  });

  afterEach(function(done) {
    Bicicleta.deleteMany({}, function(err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe('get Bicicleta/', () => {
    it('status 200', (done) => {
      request.get(base_url, function(error, response, body) {
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        //expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe('Post Bicicletas /create', () => {
    it('status 200', (done) => {
      var headers = {
        'content-type': 'application/json'
      };
      var aBici = '{"code": 10,"color": "rojo","modelo": "urbano","lat": -34,"lng": 3.2}';
      request.post({
        headers: headers,
        url: base_url + "/create",
        body: aBici
      }, function(error, response, body) {
        console.log(body);
        expect(response.statusCode).toBe(200);
        var bici = JSON.parse(body).bicicleta;
        console.log(bici);
        //expect(bici.color).toBe("rojo");
        expect(bici.ubicacion[0]).toBe(-34);
        expect(bici.ubicacion[1]).toBe(3.2);
        done();
      });
    });
  });

  

});

/*
beforeEach(() => {
  console.log('testeando…');
  Bicicleta.allBicis = [];
});

describe('Bicicleta API', () => {
  describe('Get bicicletas', () => {
    it('Status 200', () => {
      expect(Bicicleta.allBicis.length).toBe(0);
      var a = new Bicicleta(1, "Rojo", "Bmx", [4.151456, -73.638078]);
      Bicicleta.add(a);
      request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
        expect(response.statusCode).toBe(200);
      });
    });
  });
});

describe('Bicicleta API', () => {
  describe('Post bicicletas', () => {
    it('Status 200', (done) => {
      var headers = {
        'content-type': 'application/json'
      };
      var bici = '{"id":12,"modelo":"Urbano","color":"azul","lat":4.15,"lng":-73.63}';
      request.post({
        headers: headers,
        url: 'http://localhost:3000/api/bicicletas/create',
        body: bici
      }, function(error, response, body) {
        expect(response.statusCode).toBe(200);
        expect(Bicicleta.findById(12).color).toBe('azul');
        done();
      });
    });
  });
});

describe('Bicicleta API', () => {
  describe('Delete bicicletas', () => {
    it('Status 200', (done) => {
      Bicicleta.add(new Bicicleta(12, "Rojo", "Bmx", [4.151456, -73.638078]));
      expect(Bicicleta.allBicis.length).toBe(1);
      var headers = {
        'content-type': 'application/json'
      };
      request.delete({
        headers: headers,
        url: 'http://localhost:3000/api/bicicletas/delete',
        body: '{"id":12}'
      }, function(error, response, body) {
        expect(response.statusCode).toBe(204);
        expect(Bicicleta.allBicis.length).toBe(0);
        done();
      });
    });
  });
});

describe('Bicicleta API', () => {
  describe('Update bicicletas', () => {
    it('Status 200', (done) => {
      Bicicleta.add(new Bicicleta(12, "Rojo", "Bmx", [4.151456, -73.638078]));
      expect(Bicicleta.allBicis.length).toBe(1);
      var headers = {
        'content-type': 'application/json'
      };
      var bici = '{"id":12,"color":"Azul","modelo":"Urbano","lat":4.15,"lng":-73.63}';
      request.post({
        headers: headers,
        url: 'http://localhost:3000/api/bicicletas/update',
        body: bici
      }, function(error, response, body) {
        expect(response.statusCode).toBe(200);
        expect(Bicicleta.findById(12).color).toBe('Azul');
        done();
      });
    });
  });
});
*/
