var mymap = L.map('mapid').setView([4.147118, -73.636738], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoic2FudGlhZ29yb25jYW5jaW8iLCJhIjoiY2tiN2ZkMTBrMDU2MDJxbjQzc2RpOW9leSJ9.47TEeNJc76emaDfi51EvCA'
}).addTo(mymap);

var circle = L.circle([4.147118, -73.636738], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);

$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
           L.marker(bici.ubicacion,{title:bici.id}).addTo(mymap); 
        });
    }
})