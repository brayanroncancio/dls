var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: {
      type: '2dsphere',
      sparse: true
    }
  }
});

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion
  });
};

bicicletaSchema.methods.toString = function() {
  return 'code: ' + this.code + ' | color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb) {
  return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb) {
  this.create(aBici, cb);
};

bicicletaSchema.statics.findByCode = function(aCode, cb) {
  return this.findOne({
    code: aCode
  }, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode, cb) {
  return this.deleteOne({
    code: aCode
  }, cb);
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

//var a = new Bicicleta(1,"Rojo","Bmx",[4.151456, -73.638078]);
//var b = new Bicicleta(2,"Azul","Montaña",[4.149853, -73.636244]);
//var c = new Bicicleta(3,"verde","Montaña",[4.146220, -73.635884]);
